"""
Meteorology module
"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

#~ from physics.units import *
#~ from physics.types import *
#~ from physics.constants import *

#~ from physics.atm_cst import *
#~ from physics.phy_utils import *
from ouranos_meteorology.constants import *
from ouranos_meteorology.relations import *
from ouranos_meteorology.moist_air import *
from ouranos_meteorology.column import *
from ouranos_meteorology.cc_scaling import *
# ~ from meteorology.mcs import *

#~ from physics.atm import *

#~ from physics.variables import *
    
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:

        # ~ from functions import show_all, close_all
        # ~ from physics import SLP_MEAN, Temperature, CELCIUS_NM, KELVIN_NM

        # ~ close_all()
        
        print("Clausius clapeyron")

        tairs = numpy.arange(-30, 31)
        tairs = pandas.Series(tairs)
        e_s = august_roche_magnus(tairs, 'hpa')
        des = numpy.diff(e_s)
        esmn = numpy.mean([e_s[:-1], e_s[1:]], axis=0)
        cc_values = des / esmn
        print(cc_values[0] - clausius_clapeyron(tairs.iloc[0]))
        print(cc_values[-1] - clausius_clapeyron(tairs.iloc[-1]))

        
        pyplot.close("all")

        fig = pyplot.figure()
        pyplot.plot(tairs, e_s)
        pyplot.xlabel("Tair (C)")
        pyplot.ylabel("es (hpa)")
        pyplot.grid(1)
        
        


        print("temperature humidity")
        pressure = SLP_MEAN.pa
        rel_hum = 1


        for temp_celsius in range(-20, 30, 5):
            print(f"Temperature : {temp_celsius} C")
            for delta in (0, 1):
                temp_celsius = temp_celsius + delta
                temp_kelvin = Temperature(c=temp_celsius).k
                moist_air = MoistAir(t=temp_kelvin, p=pressure, rh=1)

                print(f"delta t = {delta:.1f} C")
                print(f"es = {moist_air.esat:.1f} Pa")
                print(f"mr = {moist_air.mr:.2g} kg/kg")
                print(f"v vapor = {moist_air.wvapor.rho:.2g} kg/m3")
                


        dss = MoistAir(t=291.75, pd=85255, rh=1.1)
        
        fig.show()
