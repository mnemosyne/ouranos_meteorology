"""
Class used for plotting cc scaling increasing as a function of temperature
"""
__license__ = "GNU GPL"
 
##======================================================================================================================##
##                IMPORT                                                                                                ##
##======================================================================================================================##
import argparse
import doctest


import numpy
from matplotlib import pyplot

from measurement.measures import Temperature

# ~ from physics import Temperature, KELVIN_NM, CELCIUS_NM
from ouranos_meteorology.relations import august_roche_magnus_base, clausius_clapeyron

##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

_tkmn = Temperature(c=-30).k
_tkmx = Temperature(c=40).k
#max gradient at minimal temperture values 
_ccmn = clausius_clapeyron(tkel=_tkmx)
_ccmx = clausius_clapeyron(tkel=_tkmn)

##======================================================================================================================##
##                                CC SCALING                                                                            ##
##======================================================================================================================##

class CC_Scaling:
    """ Class doc
    >>> f = pyplot.figure()
    >>> cc_sc = CC_Scaling()
    >>> xlims = [Temperature(c=i) for i in (10,50)]
    >>> lines = cc_sc.plot(y_0 = [0.1,0.2,0.5,1,2,5], xlims = xlims, ls = "--")
    >>> pyplot.grid(1)
    >>> a=pyplot.semilogy()
    >>> f.show()
    >>> import time; time.sleep(0.5)
    """
    
    def __init__(self, approximation=False, **kwarg):
        """ Class initialiser """
        self.approximation = approximation
        
        if self.approximation:
            if "value" in kwarg:
                argdic = kwarg.copy()
                ccvl = argdic.pop("value")
                
            else:
                argdic = {"tbase" : Temperature(c=15)}
                argdic.update(kwarg)
                tb = argdic.pop("tbase")
                assert isinstance(tb, Temperature), "Expect Temperature object with unit"
                t_kel = tb.k
                ccvl = clausius_clapeyron(tkel=t_kel)
            
            assert len(argdic) == 0, f"kwarg not used : {argdic}"
            assert _ccmn <= ccvl <= _ccmx, f"{ccvl:.2g} not in std val of clausius-clapeyron [{_ccmn:.2g} {_ccmx:.2g}]"
            self.approximation = float(ccvl)

            
    def approx_expr(self, tcel):
        """Can be usefull for simplification"""
        res = numpy.exp(self.approximation * tcel)
        return res
    
    def __call__(self, y_0, tcel):
        """See main doctest"""
        if self.approximation:
            res = self.approx_expr(tcel=tcel) * y_0
        else:
            res = august_roche_magnus_base(tcel=tcel) * y_0
        
        return res

    def plot(self, y_0, xlims, **kwarg):
        """See main doctest"""
        # ~ print(xlims)
        assert len(xlims) == 2, "expected bin"
        assert all(isinstance(i, Temperature) for i in xlims), "Exp Temperature class object"
        
        
        if isinstance(y_0, (list, numpy.ndarray)):
            res = [self.plot(y_0=y, xlims=xlims, **kwarg) for y in y_0]
        else:
            xlims = [t.c for t in xlims]
            ts = numpy.linspace(*xlims, num=100, endpoint=True)
            ys = self(y_0, ts)
            res = pyplot.plot(ts, ys, **kwarg)

        return res
        
        
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        
        
        
        cc_sc = CC_Scaling(approximation=False)
        cc_apv = CC_Scaling(approximation=True, value=0.07)
        cc_ap0 = CC_Scaling(approximation=True, tbase=Temperature(c=0))
        cc_ap1 = CC_Scaling(approximation=True, tbase=Temperature(c=15))
        cc_ap2 = CC_Scaling(approximation=True, tbase=Temperature(c=25))
        
        fig = pyplot.figure()
        y0 = [0.1, 0.2, 0.5, 1, 2, 5]
        
        #
        yvs = (10, 50)
        yvs = [Temperature(c=t) for t in yvs]
        cc_sc.plot(y0, yvs, ls="-", color="k", lw=3)
        cc_apv.plot(y0, yvs, ls="-.", color="r")
        cc_ap0.plot(y0, yvs, ls="--", color="g")
        cc_ap1.plot(y0, yvs, ls=":", color="c")
        cc_ap2.plot(y0, yvs, ls="--", color="orange")
        
        #
        pyplot.grid(1)
        pyplot.semilogy()
        fig.show()
