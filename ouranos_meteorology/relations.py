"""
Python module containing atmospheric relations
"""
 

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
import argparse
import doctest

import numpy
import pandas
#~ from matplotlib import pyplot

from measurement.measures import Temperature

#packages perso
from chaos_substances  import LV_WATER
from ouranos_meteorology.constants  import R_WVAPOR, PSYCHROMETRIC_CST
#, R_STAR, M_DRYAIR, R_DRYAIR, M_H2O, CP_DRYAIR, ABSOLUTE_T0, CELCIUS_NM, KELVIN_NM,
#~ from atmphysics.phy_utils import check_tempunit, transform_temperature
# ~ from physics import Temperature



##======================================================================================================================##
##                                RELATIONS                                                                             ##
##======================================================================================================================##


#VAPOR PRESSURE SATURATION #
#--------------------------#

def clausius_clapeyron(tkel):
    """
    Derivative of waper vapor given a temperature
    >>> tcel = pandas.Series([-20,-10,0,10,20,30])
    >>> tkel = tcel.apply(lambda x : Temperature(c=x).k)
    >>> des_dt = clausius_clapeyron(tkel)
    >>> ["{0:.1f}".format(i*100) for i in des_dt]
    ['8.5', '7.8', '7.3', '6.8', '6.3', '5.9']
    """
    res = LV_WATER / (R_WVAPOR * tkel ** 2)
    return res
    

def august_roche_magnus_base(tcel):
    """
    Base for clausius clapeyron computation, can be usefull for setting other origin
    >>> tairs = numpy.arange(20)
    >>> bes = august_roche_magnus_base(tairs)
    >>> dbes = numpy.diff(bes)
    >>> besmn = numpy.mean([bes[:-1], bes[1:]], axis = 0)
    >>> cc_values = dbes / besmn
    >>> ((0.06 <= cc_values) & (cc_values <= 0.08)).all()
    True
    >>> abs(cc_values[0] - clausius_clapeyron(Temperature(c=0.5).k)) < 3e-4
    True
    >>> abs(cc_values[-1] - clausius_clapeyron(Temperature(c=18.5).k)) < 1e-2
    True
    """
    #~ check_tempunit(tair, expunit = CELCIUS_NM)
    res = numpy.exp(17.625 * tcel / (tcel + 243.04))
    return res

def august_roche_magnus(tcel, unit="pa"):
    """
    A very good approximation of the Clausius-Clapeyron relation in atmopheric condition

    PARAM   :    t in celcius, unit ('pa', 'hpa')
    RETURN  :    es : saturated water vapor
    
    >>> tairs = numpy.arange(20)
    >>> es = august_roche_magnus(tairs)
    >>> des = numpy.diff(es)
    >>> esmn = numpy.mean([es[:-1], es[1:]], axis = 0)
    >>> cc_values = des / esmn
    >>> ((0.06 <= cc_values) & (cc_values <= 0.08)).all()
    True
    >>> abs(cc_values[0] - clausius_clapeyron(Temperature(c=0.5).k)) < 5e-4
    True
    >>> abs(cc_values[-1] - clausius_clapeyron(Temperature(c=18.5).k)) < 5e-3
    True
    >>> dt = 10
    >>> tairs = numpy.arange(-30,41,dt)
    >>> tairmn = numpy.mean([tairs[:-1], tairs[1:]], axis = 0)
    >>> tairs
    array([-30, -20, -10,   0,  10,  20,  30,  40])
    >>> tairmn
    array([-25., -15.,  -5.,   5.,  15.,  25.,  35.])
    >>> es = august_roche_magnus(tairs, "hpa")
    >>> des = numpy.diff(es)
    >>> esmn = numpy.mean([es[:-1], es[1:]], axis = 0)
    >>> cc_values = des / esmn / dt
    >>> ["{0:.1f}".format(i*100) for i in cc_values]
    ['8.5', '7.8', '7.2', '6.7', '6.2', '5.8', '5.4']
    
    >>> ["{0:.1f}".format(i*100) for i in clausius_clapeyron(pandas.Series(tairmn).apply(lambda x : Temperature(c=x).k))]
    ['8.8', '8.1', '7.5', '7.0', '6.5', '6.1', '5.7']
    
    >>> for t,e in zip(tairs, es): print("{0:3} : {1:.2g}".format(t,e))
    -30 : 0.51
    -20 : 1.3
    -10 : 2.9
      0 : 6.1
     10 : 12
     20 : 23
     30 : 42
     40 : 74

    
    """
    res = 6.1094 * august_roche_magnus_base(tcel) #hPa
    
    if unit == "hpa":
        pass
    elif unit == "pa":
        res = res * 10 ** 2    #Pa    
    else:
        raise  ValueError(f"Unit {unit} not yet implemented")
        
    return res


#DEWP POINT     #
#---------------#

def dewpoint_temperature(e, tamin=-50, tamax=50, dt=0.001, tol=0.1):
    """
    From water vapor pressure to dewpoint
    Doctest from https://en.wikipedia.org/wiki/Dew_point
    >>> ta = 32
    >>> es=august_roche_magnus(ta)
    >>> e = 0.73 * es
    >>> td = dewpoint_temperature(e)
    >>> "{0:.0f} : {1:.1f}".format(e, float(td))
    '3466 : 26.6'
    >>> e = 0.52 * es
    >>> td = dewpoint_temperature(e)
    >>> "{0:.0f} : {1:.1f}".format(e, float(td))
    '2469 : 20.9'
    >>> e = 0.36 * es
    >>> td = dewpoint_temperature(e)
    >>> "{0:.0f} : {1:.1f}".format(e, float(td))
    '1710 : 15.1'
    >>> e = 0.25 * es
    >>> td = dewpoint_temperature(e)
    >>> "{0:.0f} : {1:.1f}".format(e, float(td))
    '1187 : 9.5'
    >>> td = dewpoint_temperature(numpy.nan)
    >>> td = dewpoint_temperature([numpy.nan, 0, 10, 1000])
    >>> td = dewpoint_temperature(0)
    """
    tas = numpy.arange(tamin, tamax, dt)
    es = august_roche_magnus(tas)
    e = numpy.array(e, dtype=float)

    #interpolate from es to tdew
    #~ e2t = interp1d(x = es, y = tas, bounds_error = False)
    #~ res = e2t(e)
    #numpy is faster
    res = numpy.interp(e, xp=es, fp=tas, left=numpy.nan, right=numpy.nan)
    res = numpy.array(res, dtype=float)
    
    #check the results
    iok = numpy.isnan(res) == False
    rok = res[iok]
    eok = numpy.array(e)[iok]
    exp = august_roche_magnus(rok)
    dif = exp - eok
    dif = numpy.abs(dif)
    
    assert (dif < tol).all(), f"tolerance too small : max abs dif = {dif.max()}"
    assert res.shape == e.shape and res.size == e.size, "Uncoherent dim"
    
    return res
    
#HUMIDITY MESURE#
#---------------#

def relative_humidity(e, es):
    """
    Return relative humidity given actual water vapor and saturated water vapor. 
    Unit : -
    """
    res = e / es
    return res


def relative_humidity2(mr, mrs):
    """
    Hr = mr / mrs * [(P - e) / (P - es)] : here this is approximated to mr / mrs
    #from http://www.geog.ucsb.edu/~joel/g266_s10/lecture_notes/chapt03/oh10_3_01/oh10_3_01.html
    #~ Air at 1000hPa and 18oC with mixing ratio w = 6g/kg
    #~ Use skew T - ln p chart to find relative humidity and dew point
    #~ Saturation mixing ratio is 13g/kg, so RH = 100 x (6/13) = 46%
    #~ Move horizontally (constant pressure) to 6g/kg saturation mixing ratio line, Td = 6.5oC
    #~ Note: LCL is about 840hPa with temperature of about 4.5oC
    >>> pair = 1000*100
    >>> tair = 18
    >>> mr_s = 13e-3
    >>> mr = 6e-3
    >>> rh = relative_humidity2(mr = mr, mrs = mr_s)
    sligh approx when using mixing ratio to estimate relative humidity
    >>> "{0:.2f}".format(rh)
    '0.46'
    >>> es = august_roche_magnus(tair)
    >>> "{0:.0f}".format(es)
    '2060'
    >>> e  = es * rh
    >>> "{0:.0f}".format(e)
    '951'
    >>> td = dewpoint_temperature(e)
    >>> "{0:.1f}".format(float(td))
    '6.3'
    """
    print("sligh approx when using mixing ratio to estimate relative humidity")
    res = mr / mrs
    
    return res

def specific_humidity(mwater, mair):
    """
    Standar name : q
    Specific humidity : kg water / kg moist air
    """
    res = mwater / (mwater + mair)
    return res
    
def e_from_mixing_ratio(mr, ptot, tol=1, itermax=10):
    """
    Partial pressure of water vapor given mixing ratio and atm pressure 
    unit = Pa
    >>> p = 101300.0
    >>> t = 18
    >>> mr  = 6e-3
    >>> mrs = 13e-3
    >>> es = august_roche_magnus(t)
    >>> e = e_from_mixing_ratio(mr = mr, ptot = p)
    >>> "{0:.0f}".format(e)
    '967'
    >>> e1 = e_from_mixing_ratio1(mr = mr, pdry = p)
    >>> "{0:.0f}".format(e1)
    '977'
    >>> rh = relative_humidity(e=e, es=es)
    >>> "{0:.2f}".format(rh * 100)
    '46.97'
    
    
    >>> rh1 = relative_humidity(e=e1, es=es)
    >>> "{0:.2f}".format(rh1 * 100)
    '47.42'
    >>> rhtrue = relative_humidity2(mr=mr, mrs=mrs)
    sligh approx when using mixing ratio to estimate relative humidity
    >>> "{0:.2f}".format(rhtrue * 100)
    '46.15'
    >>> es2 = e_from_mixing_ratio(mr = mrs, ptot = p)
    >>> "{0:.0f}".format(es)
    '2060'
    >>> "{0:.0f}".format(es2)
    '2072'
    """
    # ~ res = mr * pair / (PSYCHROMETRIC_CST + mr)
    #~ pdry = pair - res
    #~ assert (res - e_from_mixing_ratio_and_pdry(mr = mr, pdry = pdry)) < 1e-8
    guess = e_from_mixing_ratio1(mr=mr, pdry=ptot)
    for i in range(itermax):
        # ~ print(i)
        pdry = ptot - guess
        
        res = e_from_mixing_ratio1(mr=mr, pdry=pdry)
        # ~ print(guess, res)
        guess = res
        
        if abs(res - guess) < tol:
            # ~ print('OK')
            break
        
            
    return res
    
    
def e_from_mixing_ratio1(mr, pdry):
    """
    other eq
    """
    res = mr * pdry / PSYCHROMETRIC_CST
    #~ pair = pdry + res
    #~ assert (res - e_from_mixing_ratio_and_pair(mr = mr, pair = pair)) < 1e-8
    return res
    
# ~ def e_from_mixing_ratio2(mr, ptot):
    # ~ """
    # ~ other eq
    # ~ """


def mixing_ratio(mwv, mdry):
    """
    Standar name : mr
    Return the mixing ratio (kg water/kg dry air) depending on parameter given
    >>> e_s = august_roche_magnus(30)
    >>> mr_s = mixing_ratio4(e = e_s, pair = 101300)
    >>> "{0:.1f}".format(mr_s * 1000)
    '27.2'
    """
    res = mwv / mdry
    return res
    
def mixing_ratio2(rho_wv, rho_dry):
    """
    Other eq
    """
    res = rho_wv / rho_dry
    return res
    
def mixing_ratio3(e, pdry):
    """
    Other eq : from water vapor partial pressure and dry air pressure
    """
    res = PSYCHROMETRIC_CST * e / pdry
    return res
    
def mixing_ratio4(e, pair):
    """
    Other eq: from e and moist air pressure
    """
    pdry = pair - e
    res = mixing_ratio3(e=e, pdry=pdry)
    return res
    
def mixing_ratio_5(q):
    """
    mixing ratio from specific humidity
    """
    res = q / (1-q)
    return res
    
def specific_humidity_from_mixing_ratio(mr):
    """
    specific humidity from mixing ratio
    """
    res = mr / (1 + mr)
    return res


def absolute_humidity(mwater, vair):
    """
    Return absolute humidity given water mass and air volume : 
    eq    : mwater / vair 
    unit  : kg water / m3 air
    Standar name : rho_v
    """
    res = mwater / vair
    return res
    
def absolute_humidity2(rho_dry, mr):
    """
    Compute aboslute humidity
    #not run absolute_humidity2(rho_dry = 86666, mr = 0.016)
    # not run >>> absolute_humidity2(rho_dry = 22017, mr = 0.00025)
    """
    res = rho_dry * mr    
    return res


def latent_to_sensible_heat(lat_ht, cp):
    """
    Transform latent heat to sensible heat
    res is a temperature difference
    unit = K
    """
    res = lat_ht / cp
    return res

def vpd(e, es):
    """
    Water Vapor Deficit: es - e
    """
    res = es - e
    return res

def vpd2(tcel, rh):
    """
    Vapor Pressure Deficit
    >>> round(vpd2(10, 1), 0)
    0.0
    >>> round(vpd2(10, 0.9), 0)
    123.0
    >>> round(vpd2(10, 0.8), 0)
    245.0
    >>> round(vpd2(10, 0.5), 0)
    613.0
    >>> round(vpd2(30, 1), 0)
    0.0
    >>> round(vpd2(30, 0.9), 0)
    424.0
    >>> round(vpd2(30, 0.8), 0)
    847.0
    >>> round(vpd2(30, 0.5), 0)
    2118.0
    """
    assert 0 <= rh <= 1
    es = august_roche_magnus(tcel=tcel)
    e = rh * es
    res = vpd(e=e, es=es)
    return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        tc = pandas.Series([-20, -10, 0, 10, 20, 30])
        tk = tc.apply(lambda x: Temperature(c=x).k)
