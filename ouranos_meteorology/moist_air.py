"""
Python module containing atmospheric relations
"""
__author__ = "PHYREV team"
__license__ = "GNU GPL"
__copyright__ = "Copyright 2017"
 

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
import argparse
import doctest

#~ import numpy
#~ import pandas
#~ from matplotlib import pyplot
from measurement.measures import Temperature

# ~ from obelibix.physics import R_STAR
from chaos_substances import LV_WATER, M_DRYAIR, M_H2O, CP_DRYAIR, CP_WATER_VAPOR

from hephaistos_gas import IdealGas, ALT_EQ, ALT_EQ2
from ouranos_meteorology.constants import R_WVAPOR, R_DRYAIR
from ouranos_meteorology.relations import latent_to_sensible_heat, mixing_ratio2, mixing_ratio3, relative_humidity, e_from_mixing_ratio, e_from_mixing_ratio1, august_roche_magnus, dewpoint_temperature, specific_humidity_from_mixing_ratio, mixing_ratio_5


##======================================================================================================================##
##                FUNCTION STRING                                                                                       ##
##======================================================================================================================##



##======================================================================================================================##
##                DRY AND WVAPOR                                                                                        ##
##======================================================================================================================##


class DryAir(IdealGas):
    """ Class doc
    >>> print(DryAir(p = 101300, t = 273))
    p          : 101300 (Pa)
    t          : 273.0 (K)
    rho        :  1.29 (kg/m^3)
    rgas       : 286.9 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    >>> print(DryAir(p = 101325, rho = 1.225 ))
    p          : 101325 (Pa)
    t          : 288.3 (K)
    rho        :  1.23 (kg/m^3)
    rgas       : 286.9 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    >>> print(DryAir(t = 288, rho = 1.225 ))
    p          : 101235 (Pa)
    t          : 288.0 (K)
    rho        :  1.23 (kg/m^3)
    rgas       : 286.9 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    """
    
    
    def __init__(self, **kwargs):
        """ Class initialiser """
        IdealGas.__init__(self, equation_name=ALT_EQ2, molar_mass = M_DRYAIR , **kwargs)
        assert abs(self.rgas - R_DRYAIR) <= 1e-8, f"{self.rgas} vs {R_DRYAIR}"


    def sensible_heat(self):
        """
        Sensible heat in the dry air
        Unit : J/kg of dry air
        """
        res = CP_DRYAIR * self.t
        return res


class WaterVapor(IdealGas):
    """ Class doc
    >>> print(WaterVapor(p = 2144, t = 303))
    p          :  2144 (Pa)
    t          : 303.0 (K)
    rho        : 0.0153 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    >>> print(WaterVapor(p = 2144, rho = 0.016))
    p          :  2144 (Pa)
    t          : 290.6 (K)
    rho        : 0.016 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    >>> print(WaterVapor(t = 228, p = 11 ))
    p          :    11 (Pa)
    t          : 228.0 (K)
    rho        : 0.000105 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    >>> print(WaterVapor(rho = 0.00010176, p = 11 ))
    p          :    11 (Pa)
    t          : 234.4 (K)
    rho        : 0.000102 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    """
    def __init__(self, **kwargs):
        """ Class initialiser """
        # ~ self.mgas = M_H2O
        IdealGas.__init__(self, equation_name=ALT_EQ2,  molar_mass = M_H2O, **kwargs)
        assert abs(self.rgas - R_WVAPOR) <= 1e-8, f"{self.rgas} vs {R_WVAPOR}"
        #~ print("make aliases")
        
    def latent_heat(self):
        """
        Latent heat of moist air
        Unit : J / m3 of water vapor
        """
        res = LV_WATER * self.rho
        return res

##======================================================================================================================##
##                MOIST AIR                                                                                             ##
##======================================================================================================================##

class MoistAir(IdealGas):
    """ Class doc
    >>> mb = MoistAir(t = 291.75, pd = 85255, rh = 1)
    >>> mt = MoistAir(t = 228, pd = 27716, rh = 1)
    >>> print(mb)
    ##########MOIST AIR###########
    p          : 87394 (Pa)
    t          : 291.8 (K)
    rho        :  1.03 (kg/m^3)
    rgas       : 289.6 (J/kg/K)
    molar_mass : 0.0287 (kg/mol)
    ###########DRY AIR############
    p          : 85255 (Pa)
    t          : 291.8 (K)
    rho        :  1.02 (kg/m^3)
    rgas       : 286.9 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    #########WATER VAPOR##########
    p          :  2139 (Pa)
    t          : 291.8 (K)
    rho        : 0.0159 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    ###########HUMIDITY###########
    rh         :     1 (-)
    mr         : 0.0156 (kg wv / kg da)
    t dew      :   292 (K)
    >>> print(mt)
    ##########MOIST AIR###########
    p          : 27727 (Pa)
    t          : 228.0 (K)
    rho        : 0.424 (kg/m^3)
    rgas       : 287.0 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    ###########DRY AIR############
    p          : 27716 (Pa)
    t          : 228.0 (K)
    rho        : 0.424 (kg/m^3)
    rgas       : 286.9 (J/kg/K)
    molar_mass : 0.029 (kg/mol)
    #########WATER VAPOR##########
    p          :    11 (Pa)
    t          : 228.0 (K)
    rho        : 0.000104 (kg/m^3)
    rgas       : 461.2 (J/kg/K)
    molar_mass : 0.018 (kg/mol)
    ###########HUMIDITY###########
    rh         :     1 (-)
    mr         : 0.000246 (kg wv / kg da)
    t dew      :   228 (K)
    >>> mt = MoistAir(t = 228, pd = 27716, mr = 0.0003)
    >>> mt.rh > 1
    True
    
    >>> "{0:.0f} J/kg".format(mb.sensible_heat())
    '293063 J/kg'
    >>> "{0:.0f} J/kg".format(mb.latent_heat())
    '39024 J/kg'
    >>> "{0:.0f} J/kg".format(mb.mr * LV_WATER)
    '39024 J/kg'
    >>> "{0:.2f}".format(mb.latent_heat()/ mb.sensible_heat())
    '0.13'
    >>> "{0:.0f} J/kg".format(mt.sensible_heat())
    '229026 J/kg'
    >>> "{0:.0f} J/kg".format(mt.latent_heat())
    '750 J/kg'
    >>> "{0:.0f} J/kg".format(mt.mr * LV_WATER)
    '750 J/kg'
    >>> "{0:.3f}".format(mt.latent_heat()/ mt.sensible_heat())
    '0.003'
    >>> 38 <  mb.to_dry_air().t - mb.t < 39
    True
    >>> mt.to_dry_air().t - mt.t < 1
    True
    >>> abs(mb.mr_sat() - mb.mr) < 1e-8
    True
    >>> abs(mt.mr_sat() - mt.mr) < 1e-4
    True
    >>> da  = MoistAir(t = 291.75, pd = 85255, rh = 0)
    >>> ds  = MoistAir(t = 291.75, pd = 85255, rh = 1)
    >>> dss = MoistAir(t = 291.75, pd = 85255, rh = 2)
    
    >>> da.to_saturated_air(30).t < 278
    True
    >>> da.to_dry_air().t
    291.75
    >>> f"{ds.to_saturated_air().t:.2f}"
    '291.75'
    >>> ds.to_dry_air().t > 330
    True
    >>> dss.to_saturated_air().t > 300
    True
    >>> dss.to_dry_air().t > 369
    True
    >>> abs(dss.to_saturated_air().rh - 1) < 0.005
    True
    >>> abs(dss.to_saturated_air(tolrh=0.001).rh - 1) < 0.001
    Traceback (most recent call last):
        ...
    AssertionError: niter not sufficient : 101
    rh = 0.998
    >>> abs(dss.to_saturated_air(niter=1000, tolrh=0.001).rh - 1) < 0.001
    True
    >>> dss = MoistAir(t = 291.75, pd = 85255, rh = 3)
    >>> dsat = dss.to_saturated_air()
    >>> 307 < dsat.t < 309
    True
    
    
    #####METEO FONDAMENTAUX p 67 emmagrame
    >>> moist = MoistAir(t=Temperature(c=19).k, mr=8e-3,p=1000e2)
    >>> round(moist.dewpoint().c, 1)
    10.5
    >>> round(moist.mr_sat(), 3)
    0.014
    
    >>> CP_DRYAIR
    1004.5
    >>> CP_WATER_VAPOR
    1850.0
    
    >>> f"{moist.cp():.0f}"
    '1011'
    
    """
    
    def __init__(self, t, **kwargs):
        """ Class initialiser """
        if isinstance(t, Temperature):
            tc = t.c
            t = t.k
        else:
            tc = Temperature(k=t).c
        self.esat = august_roche_magnus(tcel=tc, unit="pa")

        #~ argdry = {}
        #~ argvap = {}
        
        #need info on humidity
        if "rh" in kwargs:
            rh = kwargs.pop('rh')
            assert rh >= 0, "relative humidity out of the range must be > 0"
            assert rh < 10, "relative humidity out of the range must be < 10"
            #~ if rh > 1:
                #~ print("Warning: sursaturated moist air : rh = {0:.2f}".format(rh))
            
            e = rh * self.esat
            mr = None
            #~ argvap["p"] = e
        elif 'mr' in kwargs:
            mr = kwargs.pop('mr')
            e = None
        elif "e" in kwargs:
            e = kwargs.pop("e")
            mr = None
        elif "q" in kwargs:
            q = kwargs.pop("q")
            mr = mixing_ratio_5(q)
            e = None
            
        else:
            raise ValueError("lack information on humidity")
        
        if 'p' in kwargs:
            ptot = kwargs.pop("p")
            if e is None:
                if mr is not None:
                    e = e_from_mixing_ratio(mr=mr, ptot=ptot)
                else:
                    raise ValueError("not implemented")
            
            pa = ptot - e
        elif "pd" in kwargs:
            pa = kwargs.pop("pd")
            if e is None:
                if mr is not None:
                    e = e_from_mixing_ratio1(mr=mr, pdry=pa)
                else:
                    raise ValueError("not implemented")
            ptot = pa + e

        else:
            raise ValueError("lack information on dry air pressure")
        
        
        assert len(kwargs) == 0, f"remaing kwargs\ndont know what to do with : {kwargs}"
       
        self.dryair = DryAir(p=pa, t=t)
        self.wvapor = WaterVapor(p=e, t=t)
        
        rho = self.dryair.rho + self.wvapor.rho
        # ~ raise ValueError('update must be in ideal gas')
        # ~ r_moist = ptot / (rho * t)

        #~ print(ptot, t)
        IdealGas.__init__(self, equation_name=ALT_EQ, p=ptot, t=t, rho=rho)
        
        drho = (self.rho - rho)
        assert drho < 1e-8, f"pb code, drho={drho}"
        
        self.rh = relative_humidity(e=self.wvapor.p, es=self.esat)
        self.mr = mixing_ratio2(rho_wv=self.wvapor.rho, rho_dry=self.dryair.rho)
        self.q = specific_humidity_from_mixing_ratio(mr=self.mr)
        
        
    def __str__(self):
        """
        For print function
        See main doctest
        """
        moist_str = IdealGas.__str__(self)
        dry_str = self.dryair.__str__()
        wvapor_str = self.wvapor.__str__()
        
        humkys = ['rh', 'mr', "t dew"]
        humvals = [self.rh, self.mr, float(self.dewpoint().k)]
        humunits = ["-", "kg wv / kg da", "K"]
        hum_str = [f"{k:10} : {v:5.3g} ({u})" for k, v, u in zip(humkys, humvals, humunits)]
        hum_str = "\n".join(hum_str)
        
        txts = []
        for nm, txt in zip(["moist air", "dry air", "water vapor", "humidity"], [moist_str, dry_str, wvapor_str, hum_str]):
            txts += [f"{nm:#^30}".upper(), txt]
        
        #~ print(txts)
        res = "\n".join(txts)
        
        return res
        
    def sensible_heat(self):
        """See overhidden method for dry air"""
        res = self.dryair.sensible_heat()
        return res
    

    def latent_heat(self):
        """Latent heat of moist air
        Unit : J / kg of dry air
        """
        res = self.wvapor.latent_heat() /self.dryair.rho #eq to self.mr * LV_WATER
        return res

    def mr_sat(self):
        """Returnsaturation mixing ratio"""
        res = mixing_ratio3(e=self.esat, pdry=self.dryair.p)
        return res
        
    def dewpoint(self):
        """Compute dew point temperature"""
        res = dewpoint_temperature(e=self.wvapor.p)
        res = Temperature(c=res)
        return res
        

    def to_dry_air(self):
        """Transform a moist air to a dry air : latent heat release, --> sensible heat"""
        lat_ht = self.latent_heat()
        dtlat = latent_to_sensible_heat(lat_ht=lat_ht, cp=CP_DRYAIR)
        new_t = self.t + dtlat
        res = DryAir(p=self.p, t=new_t)
        return res
        
    def to_saturated_air(self, niter=100, d_iter=0.2, tolrh=0.005):
        """
        Transform the sursaturated water vapor to latent heat, then sensible heat of dry : precipitation heating
        Or increase humidity until saturatrion is reached by taking heat of dry air : evaporation
        """
        assert 0 < d_iter < 1
        if niter == 1:
            dmr = self.mr - self.mr_sat()                                #if rh > 1 then it is positive else negative
            dmr = dmr * d_iter
            dlatht = dmr * LV_WATER                                      # add sensible heat if rh > 1
            dtlat = latent_to_sensible_heat(lat_ht=dlatht, cp=CP_DRYAIR) # tranforme water vapor latent energy to sensible air temperature energy
            new_t = self.t + dtlat
            new_mr = self.mr - dmr
            #~ assert new_mr == self.mr_sat()
            res = MoistAir(t=new_t, p=self.p, mr=new_mr)
        else:
            res = self.to_saturated_air(niter=1, d_iter=d_iter)
            iiter = 0
            cond = True
            while iiter <= niter and cond:
                res = res.to_saturated_air(niter=1, d_iter=d_iter)
                iiter += 1
                cond = abs(res.rh - 1) > tolrh
                
            assert not cond, f"niter not sufficient : {iiter}\nrh = {res.rh:.3g}"
        
        return res

    def cp(self):
        """Specific heat of humid air |normal atm at cst P
        For 1kg of dry air we have mr kg of water vapor
        Thus 1+self.mr kg of moist air
        To increase this of 1 joule: It require cpdryair J and self.mr cpwvapor J
        """
        dj = CP_DRYAIR + self.mr * CP_WATER_VAPOR
        dm = 1 + self.mr
        res = dj/dm
        return res
        
        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
