"""
Python module containing atmospheric relations
"""
__author__ = "PHYREV team"
__license__ = "GNU GPL"
__copyright__ = "Copyright 2017"

 

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##

#~ import math
#~ import numpy
from numbers import Number
import doctest
import argparse
import numpy
import pandas
from scipy.interpolate import interp1d



from measurement.measures import Temperature
#~ from atmphysics.phy_utils import check_tempunit
from apollon_physics import GRAV_ACCEL# PhysicVar, GRAV_ACCEL, Temperature #RHO_WATER, LV_WATER
from chaos_substances import CP_DRYAIR
from ouranos_meteorology.constants import STD_LAPSERATE, STD_P0, STD_T0, R_DRYAIR#, DRY_ADIAB_LAPSE_RATE
from ouranos_meteorology.moist_air import MoistAir, DryAir #IdealGas,


##======================================================================================================================##
##                STD ATMOSPHERE CONSTANTS                                                                              ##
##======================================================================================================================##



#~ STD_PARTICLE_Z0 = DryAir()

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class AirParticle(MoistAir):
    """ Standard atmosphere
    t_0  : temperature at 0 meter
    p_0  : pressure at 0 meter
    lapse_rate : dt / dz
    
    >>> z0 = 0
    >>> z1 = 1000
    >>> z2 = 3000
    >>> std_atm = AirParticle(z = z0, rh = 0.5)
    >>> ap0 = std_atm.lapse_rate_lift(z = 0)
    >>> ap0.t
    288.15
    >>> "{0:.0f}".format(ap0.esat)
    '1702'
    >>> abs(ap0.rh - 0.5) < 1e-4
    True
    >>> ap1 = std_atm.lapse_rate_lift(z1)
    >>> ap1.t
    281.65
    >>> ap1.z
    1000
    >>> "{0:.2f}".format(ap1.rh)
    '0.68'
    
    >>> "At {0} m, p = {1:.0f} Pa".format(z1, ap1.p)
    'At 1000 m, p = 89901 Pa'
    >>> ap2 = std_atm.lapse_rate_lift(z2)
    >>> ap2.rh > 1
    True
    >>> ap2.t
    268.65
    >>> ap2.z
    3000
    >>> "{0:.2f}".format(ap2.rh)
    '1.35'
    >>> "At {0} m, p = {1:.0f} Pa".format(z2, ap2.p)
    'At 3000 m, p = 70172 Pa'
    
    >>> p0 = std_atm.p
    >>> p1 = ap1.p
    >>> p2 = ap1.p
    
    >>> ap0b = std_atm.dry_adiabatic_lift(p0)
    >>> ap0b.t
    288.15
    >>> ap3 = std_atm.dry_adiabatic_lift(p1)
    >>> grad_adiab = (ap3.t - ap0.t) / z1
    >>> "Adiabatic gradient with standard atmsphere pressure and lapse rate : {0:.2g} °/km".format(grad_adiab * 1000)
    'Adiabatic gradient with standard atmsphere pressure and lapse rate : -9.7 °/km'
    >>> "{0:.0f} J/kg".format(ap0.dry_static_energy())
    '289447 J/kg'
    
    >>> ap0.dry_static_energy() == ap3.dry_static_energy()
    True
    >>> ap0.dry_static_energy() < ap1.dry_static_energy()
    True
    >>> "{0:.2f}".format(ap1.potential_temperature())
    '291.44'
    >>> "{0:.2f}".format(ap2.potential_temperature())
    '298.38'
    >>> "{0:.2f}".format(ap3.potential_temperature())
    '288.15'
    
    >>> std_atm = AirParticle(z = z0, rh = 1)
    >>> apdry = std_atm.dry_adiabatic_lift(p1)
    >>> 277 < apdry.t < 279
    True
    
    >>> "{0:.2f}".format(apdry.rh)
    '1.69'
    >>> apmoist = std_atm.moist_adiabatic_lift(p1)
    >>> 282 < apmoist.t < 284
    True
    >>> "{0:.2f}".format(apmoist.rh)
    '1.00'
    >>> gradmoist = (apmoist.t - std_atm.t) / z1
    >>> "{0:.1f} C/km".format(gradmoist * 1000)
    '-4.9 C/km'
    >>> apmoist.moist_static_energy() - std_atm.moist_static_energy() < 1e-8
    True
    
    #####METEO FONDAMENTAUX p 48 emmagrame
    >>> ap = AirParticle(t=Temperature(c=30), mr=20e-3,p=1000e2, z=0) 
    
    >>> round(Temperature(k=ap.dry_adiabatic_lift(800e2).t).c,1)
    11.3
    >>> round(Temperature(k=ap.dry_adiabatic_lift(700e2).t).c,1)
    0.6
    >>> round(Temperature(k=ap.dry_adiabatic_lift(600e2).t).c,1)
    -11.2
    
    >>> dew_air = ap.dry_adiabatic_lift_until_saturation()
    >>> round(Temperature(k=dew_air.t).c, 1)
    23.4
    >>> round(dew_air.p/100, 0) == 926.0
    True
    >>> lf1 = dew_air.moist_adiabatic_lift(p=480e2)
    >>> round(Temperature(k=lf1.t).c, 0)
    1.0
    >>> round(lf1.z, -2) == 5900.0
    True
    >>> lf2 = dew_air.moist_adiabatic_lift(p=260e2)
    >>> round(Temperature(k=lf2.t).c, 0)
    -23.0
    >>> round(lf2.z, -2) == 9900.0
    True
    
    #####METEO FONDAMENTAUX p 67 emmagrame
    >>> ap = AirParticle(t=Temperature(c=19).k, mr=8e-3,p=1000e2, z=0)
    >>> round(Temperature(k=ap.dry_adiabatic_lift(p=900e2).t).c, 1)
    10.3

    >>> round(Temperature(k=ap.dry_adiabatic_lift(p=850e2).t).c, 1)
    5.7

    >>> round(Temperature(k=ap.dry_adiabatic_lift(p=800e2).t).c, 1)
    1.0

    >>> dew_air = ap.dry_adiabatic_lift_until_saturation()
    >>> round(Temperature(k=dew_air.t).c, 1)
    8.6
    >>> round(dew_air.p/100, -1) == 880.0
    True
    
    
    #####METEO FONDAMENTAUX p 72 emmagrame
  
    >>> ap = AirParticle(t=Temperature(c=5), mr=3.5e-3,p=700e2, z=3000)
    >>> dl1 = ap.dry_adiabatic_lift(p=590e2)
    >>> round(Temperature(k=dl1.t).c, 1)
    -8.3
    >>> round(dl1.p, 1)
    59000.0
    >>> round(dl1.rh, 1) == 1.0
    True
    
    >>> dl1 = ap.pseudo_adiabatic_lift(p=590e2)
    >>> round(Temperature(k=dl1.t).c, 1)
    -8.2
    >>> round(dl1.p, 1)
    59000.0
    >>> round(dl1.rh, 1) == 1.0
    True
    
    
    >>> dl2 = ap.pseudo_adiabatic_lift(p=500e2)
    >>> round(Temperature(k=dl2.t).c, 1)
    -16.9
    >>> round(dl2.p, 1)
    50000.0
    >>> round(dl2.rh, 1) == 1.0
    True
    >>> round(dl2.mr, 4)
    0.0021
    
    
    """
    
    def __init__(self, z, p=STD_P0.pa, t=STD_T0, **kwarg):
        """ Class initialiser """
        #~ check_tempunit(t_0, KELVIN_NM)
        if len(kwarg) == 0:
            MoistAir.__init__(self, p=p, t=t, rh=0) #dry air
        else:
            MoistAir.__init__(self, p=p, t=t, **kwarg)

        assert isinstance(z, Number), f"exp number, got {z.__class__} : {z}"
        self.z = z
        
        #~ self.lapse_rate = lapse_rate
        #~ assert isinstance(airz0, IdealGas), "exp az, got {0.__class__}\n{0}".format(airz0)
        #~ self.air_z0     = airz0
        

    def lapse_rate_lift(self, z, lapse_rate=STD_LAPSERATE, **kwarg):
        """
        Return air air particle corresponding to the given level
        Kwarg are used to specify humidity information
        z    : elevation (m)
        kwarg : humidity info at level z
        """
        assert 0 < lapse_rate < 1e-2, "lapse rate must be in m/m"
        assert isinstance(z, Number)
        #~ assert 0, "use particule Z0"
        dz = z - self.z
        t_0 = self.t
        tair = t_0 - lapse_rate * dz
        tratio = tair / t_0

        power = GRAV_ACCEL / (self.rgas * lapse_rate)
        
        p_0 = self.p
        pair = p_0 * tratio ** power
        
        if len(kwarg) > 0:
            argdic = kwarg.copy()
        else:
            argdic = {"mr" : self.mr}
        
        res = AirParticle(z=z, p=pair, t=tair, **argdic)
        #~ else:
            #~ res = DryAir(p = pair, t = tair)
        
        return res

    def dry_adiabatic_lift(self, p):
        """
        #see https://fr.wikipedia.org/wiki/Compression_et_d%C3%A9tente_adiabatique for explanations about formulae
        #https://fr.wikipedia.org/wiki/Gradient_thermique_adiabatique
        #http://www.meteofrance.fr/publications/glossaire/149256-adiabatique
        r / cp = (gamma - 1) / gamma --> gamma = cp/cv see
        #http://www.lmd.jussieu.fr/~fcodron/COURS/notes_thermo.pdf
        See main doctest
        Or write the three equations : static energy conservation, perfect gases, hydrostatic
        """
        power = R_DRYAIR / CP_DRYAIR
        pratio = p / self.p
        tadiab = self.t * pratio ** power
        #~ drynrj = self.dry_static_energy()
        #~ print(p,)
        gas = DryAir(p=p, t=tadiab)
        #~ print(gas)
        d_nrj = self.sensible_heat() - gas.sensible_heat()
        #~ print(d_nrj)
        dz = d_nrj / GRAV_ACCEL
        z = self.z + dz
        
        res = AirParticle(z=z, p=p, t=tadiab, mr=self.mr)
        #~ res = self.lapse_rate_lift(z = z, lapse_rate = DRY_ADIAB_LAPSE_RATE, mr = self.mr)
        #~ if self.rh > 1:
            #~ print("Warning : rel humidity > 1: {0:.1f}".format(self.rh))

        return res
        
        
    def moist_adiabatic_lift(self, p, tolrh=0.005, **kwargs):
        """
        Moist adiabatic lift
        """
        
        assert abs(self.rh - 1) < tolrh, "must be saturated"
        
        #first guess
        fguess = self.dry_adiabatic_lift(p=p)
        satair = fguess.to_saturated_air(**kwargs)
        assert satair.p == p
        res = AirParticle(z=fguess.z, p=satair.p, t=satair.t, rh=satair.rh)
        return res

    def dry_adiabatic_lift_until_saturation(self, **kwargs):
        """Lift air until saturation"""
        assert self.rh < 1, "already saturated"
        
        argdic = dict(niter=20, tolrh=0.005, nspace=4, p_top=1e4)
        argdic.update(kwargs)
        
        niter = argdic["niter"]
        tolrh = argdic["tolrh"]
        nspace = argdic["nspace"]
        p_top = argdic['p_top'] #p at the top, used for linspace, at this pressure, rh > 1
        p_btm = self.p #p bottom use for linspace, at this pressure rh < 1
        
        wanted = 1
        
        for _ienum in range(niter):
            # ~ print(p_top, p_btm)
            ps = numpy.linspace(p_top, p_btm, nspace)
            ps = list(ps)
            rhs = [self.dry_adiabatic_lift(p=p).rh for p in ps]
            #inter1D
            f1d = interp1d(x=rhs, y=ps)
            guess = f1d(wanted)

            guess = self.dry_adiabatic_lift(p=guess)
                    
            if abs(guess.rh - wanted) < tolrh: 
                # ~ print(guess.p, guess.rh)
                # ~ print("stop")
                break

            ps = ps + [int(guess.p)]
            rhs = rhs + [guess.rh]            
            ts = pandas.Series(rhs, index=ps)
            #sperate sup and inf to rh=1
            rh_inf = ts[ts<wanted]
            rh_sup = ts[ts>wanted]
            #get corresponding pressure levels
            p_btm = rh_inf.index.min() #take the higher altitude, thus the lower pressure for which rh<1
            p_top = rh_sup.index.max() # #take the lower altitude, thus the higer pressure for which rh>1
            
        
        assert abs(guess.rh - wanted) < tolrh, f"optimisation failed {guess.rh}"
        satair = guess.to_saturated_air(tolrh=tolrh)
        assert (satair.t - guess.t) < 1e-2 and (satair.p - guess.p) < 1e-2
        assert abs(satair.rh - wanted) <= tolrh/2
        res = AirParticle(z=guess.z, p=satair.p, t=satair.t, rh=wanted)
        assert abs(res.rh - wanted) < tolrh, "optimisation failed"
        
        return res


    # ~ def new_dry_adiabatic_lift_until_saturation(self, **kwargs):
        # ~ """Lift air until saturation"""
        # ~ assert self.rh < 1, "already saturated"
        
        # ~ argdic = dict(niter=20, tolrh=0.005, dp=1000, e_dp = 2)
        # ~ argdic.update(kwargs)
        
        # ~ niter = argdic["niter"]
        #d_rh = argdic["d_rh"]
        # ~ tolrh = argdic["tolrh"]
        # ~ dp = argdic["dp"]
        #nspace = argdic["nspace"]
        
        # ~ e_dp = argdic["e_dp"]
        
        # ~ wanted = 1
        # ~ lift_btm = self #lift bottom
        
        
        # ~ for _ienum in range(niter):
            #print("iter", ienum)
            # ~ lift_top = lift_btm
            # ~ while lift_top.rh < wanted:
                # ~ lift_btm = lift_top
                # ~ plift_top = lift_btm.p - dp
                # ~ lift_top = lift_btm.dry_adiabatic_lift(p=plift_top)
                # ~ print(lift_top.p, lift_top.rh)
            
            # ~ lift_btm = lift_top
            # ~ dp = dp / e_dp
            
            # ~ while lift_btm.rh > wanted:
                # ~ lift_top = lift_btm
                # ~ plift_btm = lift_top.p + dp
                # ~ lift_btm = lift_top.dry_adiabatic_lift(p=plift_btm)
                #lift_btm = lift_top
                # ~ print(lift_btm.p, lift_btm.rh)
            
            # ~ dp = dp / e_dp


            
            # ~ guess = numpy.interp(wanted, xp=[lift_btm.rh, lift_top.rh], fp=[lift_btm.p, lift_btm.p], left=None, right=None) #rh_inf give higher pressure thus inversed
            # ~ guess = self.dry_adiabatic_lift(p=guess)
            
            # ~ if abs(guess.rh - wanted) < tolrh: 
                #print(guess)
                # ~ break
        
            # ~ if guess.rh < wanted:
                # ~ lift_btm = guess
                
            
            #print(dp)
        
        # ~ assert abs(guess.rh - wanted) < tolrh, f"optimisation failed {guess.rh}"
        # ~ satair = guess.to_saturated_air(tolrh=tolrh/5)
        # ~ assert (satair.t - guess.t) < 1e-2 and (satair.p - guess.p) < 1e-2
        # ~ assert abs(satair.rh - wanted) <= tolrh/2
        # ~ res = AirParticle(z=guess.z, p=satair.p, t=satair.t, rh=wanted)
        # ~ assert abs(res.rh - wanted) < tolrh, "optimisation failed"
        
        # ~ return res

    # ~ def old_dry_adiabatic_lift_until_saturation(self, **kwargs):
        # ~ """Lift air until saturation"""
        # ~ assert self.rh < 1, "already saturated"
        
        # ~ argdic = dict(niter=20, d_rh=0.5, tolrh=0.005, dp=0.1, nspace = 5, e_rh = 0.9)
        # ~ argdic.update(kwargs)
        
        # ~ niter = argdic["niter"]
        # ~ d_rh = argdic["d_rh"]
        # ~ tolrh = argdic["tolrh"]
        # ~ dp = argdic["dp"]
        # ~ nspace = argdic["nspace"]
        
        # ~ e_rh = argdic["e_rh"]
        
        # ~ wanted = 1
        
        
        # ~ for ienum in range(niter):
            # ~ d_rh = d_rh * e_rh ** ienum
            # ~ rh_inf = wanted - d_rh
            # ~ rh_sup = wanted + d_rh
            
            # ~ if ienum == 0:
                # ~ sup = self.p
                # ~ inf = sup * dp #diminsih pressure, thus lift air higher
            
            # ~ ps = numpy.linspace(sup, inf, nspace)
            # ~ rhs = [self.dry_adiabatic_lift(p=p).rh for p in ps]
            #print(ps)
            #print(rhs)
            
            # ~ inf, guess, sup = numpy.interp([rh_sup, wanted, rh_inf], xp=rhs, fp=ps, left=None, right=None) #rh_inf give higher pressure thus inversed
            
            #print(inf, sup)
            
            # ~ guess = self.dry_adiabatic_lift(p=guess)
            
        
            # ~ if abs(guess.rh - wanted) < tolrh: 
                #print(guess)
                # ~ break
        
        # ~ assert abs(guess.rh - wanted) < tolrh, "optimisation failed {0}".format(guess.rh)
        # ~ satair = guess.to_saturated_air(tolrh=tolrh, niter=200)
        # ~ assert (satair.t - guess.t) < 1e-4 and (satair.p - guess.p) < 1e-4
        # ~ assert abs(satair.rh - wanted) <= tolrh/2
        # ~ res = AirParticle(z=guess.z, p=satair.p, t=satair.t, rh=wanted)
        # ~ assert abs(res.rh - wanted) < tolrh, "optimisation failed"
        
        # ~ return res
        
        
    def pseudo_adiabatic_lift(self, p):
        """Pseudo adiatic"""
        guess = self.dry_adiabatic_lift_until_saturation()
        if guess.p < p:
            res = self.dry_adiabatic_lift(p=p)
        else:
            res = guess.moist_adiabatic_lift(p=p)
            
        return res
        
        
        
    def potential_energy(self):
        """
        Potential energy
        Unit : J/kg of dry air
        """
        res = GRAV_ACCEL * self.z
        return res
        
        
    def dry_static_energy(self):
        """
        Compute the dry static energy
        Unit : J/kg of dry air
        Dry static energy is conservated during dry adiabatic ascent
        See main doctest
        """
        pot = self.potential_energy()
        sen = self.sensible_heat()
        res = pot + sen
        return res
        
    def moist_static_energy(self):
        """
        Compute moist static energy
        Moist static energy is conservated during moist adiabatic ascent
        See main doctest        
        """
        dry_engy = self.dry_static_energy()
        lat_ht = self.latent_heat()
        res = dry_engy + lat_ht
        return res
        
    def potential_temperature(self, p=STD_P0.pa):
        """
        Return potential temperauture of a virtual air parcel following a dry adiabatic 
        See main doctest
        """
        power = R_DRYAIR / CP_DRYAIR
        pratio = p / self.p
        
        res = self.t * pratio ** power
        return res
        
        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    parser.add_argument('--td', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        pass
        
        



    #~ std_atm = AirParticle(z = z0, rh = 1)
    
    z0 = 0
    z1 = 1000
    std_atm = AirParticle(z=z0, rh=1)
    
    
    #####METEO FONDAMENTAUX p 67 emmagrame
    ap = AirParticle(t=Temperature(c=19), mr=8e-3,p=1000e2, z=0)
    dew_air = ap.dry_adiabatic_lift_until_saturation()
    # ~ print(Temperature(k=dew_air.t).c, 1)
    # ~ print(dew_air.p/100, 0)
    ap1 = AirParticle(t=Temperature(c=19), rh=0.2,p=1000e2, z=0)

    
#~ ap0 = std_atm.lapse_rate_lift(z = 0)
#~ ap0.t
#~ 288.15
#~ ap1 = std_atm.lapse_rate_lift(z1)
#~ ap1.t
#~ 281.65
#~ ap1.z
#~ 1000
#~ "At {0} m, p = {1:.0f} Pa".format(z1, ap1.p)
#~ 'At 1000 m, p = 89867 Pa'
#~ ap2 = std_atm.dry_adiabatic_lift(z0)
#~ ap2.t
#~ 288.15
#~ ap3 = std_atm.dry_adiabatic_lift(z1)
#~ grad_adiab = (ap3.t - ap0.t) / z1
#~ "Adiabatic gradient with standard atmsphere pressure and lapse rate : {0:.2g} °/km".format(grad_adiab * 1000)
#~ 'Adiabatic gradient with standard atmsphere pressure and lapse rate : -9.8 °/km'
#~ ap0.dry_static_energy(z=0)

#~ ap0.dry_static_energy(z=0) == ap3.dry_static_energy(z=z1)
#~ True
#~ ap0.dry_static_energy(z=0) < ap1.dry_static_energy(z=z1)
#~ True



#~ dp = 1

#~ for p in numpy.arange(100e2, 1000e2, 100e2):
    #~ a0 = AirParticle(z=0,p = p)
    #~ a1 = a0.dry_adiabatic_lift(p=p+dp)
    #~ dz = a1.z - a0.z
    #~ dt = a1.t - a0.t
    #~ print("dry", dt / dz)

#~ for t in numpy.arange(230,311,20):
    #~ for p in numpy.arange(500e2, 1001e2, 100e2):
        #~ a0 = AirParticle(z=0,p = p, t=t, rh = 1)
        #~ a1 = a0.moist_adiabatic_lift(p=p+dp)
        #~ dz = a1.z - a0.z
        #~ dt = a1.t - a0.t
        #~ print("moist", t, p,  (dt / dz)*1000)
    
