"""
 Meteorological Constants
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

from measurement.measures import Temperature, Pressure


from apollon_physics import PhysicConstant, GRAV_ACCEL
from chaos_substances import M_H2O, M_DRYAIR, CP_DRYAIR
from hephaistos_gas import rgas_from_molar_mass
# ~ from obelibix.meteorology import 

##======================================================================================================================##
##               SET CONSTANTS                                                                                          ##
##======================================================================================================================##

#GAS CONSTATS   #
#---------------#

R_DRYAIR = PhysicConstant(name="Rd", value=rgas_from_molar_mass(M_DRYAIR), unit="J/kg/K", infos="R*/Md : spec air gas constant")
R_WVAPOR = PhysicConstant(name="Rv", value= rgas_from_molar_mass(M_H2O), unit="J/kg/K", infos="R*/Mwater : spec water vapor constant")


PSYCHROMETRIC_CST = M_H2O / M_DRYAIR       # 0.622


#PRESSURE       #
#---------------#
SLP_MEAN = Pressure(atm=1) #="Mean Sea Level Pressure"


STD_P0 = SLP_MEAN #.pa #"standard pressure at 0m")  # 1atm
STD_T0 = Temperature(k=288.15) #"standard temperature at 0m") #15°C

STD_LAPSERATE = PhysicConstant(value=6.5e-3, name="dt/dz", unit="K/m", infos="standard lapse rate") #6.5 °C/km

_alr = GRAV_ACCEL / CP_DRYAIR #~~ 9.8 °C/km
DRY_ADIAB_LAPSE_RATE = PhysicConstant(value=_alr, name="dt/dz|adiab", unit="K/m", infos="adiabatic lapse rate of dry air")


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    
    print(R_DRYAIR)
    print(R_WVAPOR)
    print(PSYCHROMETRIC_CST)
